**To download this mod, please go to the [Mod Releases Page](https://gitlab.com/dragonflame-studios/total-miner-mod-firearms-miscellanea/-/releases)**

Firearms Miscellanea is a simple XML mod for the former Xbox 360, and now PC game Total Miner.

The goal of this mod is to add in various firearm-related items, namely various ammo types for the different games.
The mod adds the following items into the game:

- Gunpowder (Crafted from 2 Carbon and 2 Sulfur)
- Casing Primers (Crafted from a Bronze Ingot, Flint Flake, and Iron Ingot)
- Various Casing and Bullet Molds (Purchaseable in the shops for 300gp each)
- Casings and Bullets (Crafted in a furnace with their respective mold and either Bronze or Iron Ingots.)
- Pistol, Revolver, Light/Heavy Rifle, and Shotgun ammo. (Crafted from 1-3 gunpowder, 1 primer, 1 appropriate casing, and the appropriate bullet <Or BBs for 2/3 shotgun shells>)


The items in this mod, outside of being used for scripts, have no functionality.
